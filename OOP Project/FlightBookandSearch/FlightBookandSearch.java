package FlightBookandSearch;	//package FlightSearchFinal

//importing other classes by setting their classpath in environment variables
import FlightSearch.FlightSearch;	
import BookTicketSearch.BookTicketSearch;
import DirectUserSearch.DirectUserSearch;
import java.util.*;
import java.io.*;


class FlightBookandSearch	//main class FlightSearchFinal
{
	public static void displayList()	//displayList function to display list of cities
	{
		String []cities = {"Chennai", "Bangalore", "Delhi", "Ahmedabad", "Mumbai"};
		int i;
		System.out.print("\n");
		for(i=0; i<cities.length; i++)
		{
			System.out.print("\t");
			System.out.println(i+1 + ". " + cities[i]);
		}
		System.out.print("\n");
	}
	public static void displayTime()	//displayTime function to display list of timings
	{
		String []time = {"00:00", "4:00", "8:00", "12:00", "16:00", "20:00"};
		int i;
		System.out.print("\n");
		for(i=0; i<time.length; i++)
		{
			System.out.print("\t");
			System.out.println(i+1 + ". " + time[i]);
		}
		System.out.print("\n");
	}
	public static void displayMonth()	//displayMonth function to display list of months
	{
		String []months = {"Jan", "Feb", "Mar", "Apr", "May"};
		int i;
		System.out.print("\n");
		for(i=0; i<months.length; i++)
		{
			System.out.print("\t");
			System.out.println(i+1 + ". " + months[i]);
		}
		System.out.print("\n");
	}
	public static void main(String args[])	//main function
	{		
		Scanner input = new Scanner(System.in);
		byte choice, choiceX=0, choiceY, choiceMonth, choiceTime, choiceAirway, choiceInput;	//bytes for selecting various cases
		String fromY=null, toY=null, mobileNoY=null, userNameY=null, passwordY=null, startMonthY=null, startTimeY=null;	//initiaslisation of string variables
		boolean bookTicketChoiceY;	//boolean data type since ticket choice has either true or false value
		char genderY=0, bookTicketChar;	//initiaslisation of char variables
		boolean bookTicketBoolean;	//boolean data type since ticket choice has either true or false value
		int refNoY = 0, flag = 0;
		FlightSearch f = new DirectUserSearch(refNoY);	//intialisation of DirectUserSearch class and refering it to it's parent class FlightSearch
		do
		{	
			System.out.println("\n\n************** Welcome to AIRLINE RESERVATION SYSTEM ************\n");
			System.out.print("0. Terminate \n1. Client Section \n2. Administrator Section \n3. View Customs Information \n\n");
			System.out.print("______________________________________________________\n");
			System.out.print("\nEnter your choice: ");
			choiceInput = input.nextByte();
			System.out.print("______________________________________________________\n");
			if(choiceInput == 1)	//case 1: Client Section
			{
				System.out.print("______________________________________________________\n");
				System.out.print("\n\t0. Terminate \n\t1. Direct Search without login \n\t2. Book ticket with search \n\t3. View Direct User Search History \n\t4. View Booked ticket History \n\n");
				System.out.print("______________________________________________________\n");
				System.out.print("\n->\tEnter your choice: ");
				choice = input.nextByte();
				System.out.print("______________________________________________________\n");
				refNoY++;
				if(choice == 1)	//case 1: Direct Search without login
				{
					f = new DirectUserSearch(refNoY);	//making f refer to the instance of DirectUserSearch
				}
				if(choice == 2)	//case 2: Book Ticket with search
				{
					f = new BookTicketSearch(refNoY);	//making f refer to instance of BookTicketSearch
					System.out.print("______________________________________________________\n");
					System.out.println("\n****************** SIGNUP INFO ********************\n");
					System.out.print("______________________________________________________\n");
					/* getting details from user for username, password and such other things for book ticket search */
					System.out.print("\n\tEnter UserName: ");
					userNameY = input.next();
					System.out.print("\tEnter Password: ");
					passwordY = input.next();
					System.out.print("\tMobile No: ");
					mobileNoY = input.next();
					System.out.print("\tGender: ");
					genderY = input.next().charAt(0);
				}
				if(choice!=0 && choice!=3 && choice!=4)	//for cases 1 and 2 flight search algorithm would be common therefore this condition is implemented while it is not for cases 3 and 4
				{
					System.out.print("______________________________________________________\n");
					System.out.println("\n*************** FLIGHT SELECTION *****************\n");
					System.out.print("______________________________________________________\n");
					System.out.println("\n**************************************************");
					System.out.print("*\t YOUR REFERENCE NO IS  " + refNoY + "\t \t*\n");
					System.out.println("**************************************************\n");
					System.out.println("\n\tFrom: ");
					displayList();
					System.out.print("______________________________________________________\n");
					System.out.print("\n->\tEnter your choice: ");
					choiceX = input.nextByte();
					System.out.print("______________________________________________________\n");
					//selecting from and to place as per user's choice
					switch(choiceX)
					{
						case 1:	//case 1 for flights departing form Chennai
						fromY = "Chennai";
						for(; ; )	//infinite for loop so that it terminates only when user enters valid input
						{
							flag=1;	//initializing flag with 1
							System.out.print("______________________________________________________\n");
							System.out.println("\n \tTo: ");
							displayList();
							System.out.print("______________________________________________________\n");
							System.out.print("\n->\tEnter your choice: ");
							choiceY = input.nextByte();
							System.out.print("______________________________________________________\n");
							switch(choiceY)
							{
								//various cases for arrival of flights at different cities
								case 1:
								System.out.println("\n\t\tSOURCE AND DESTINATION PLACE CANNOT BE SAME.");
								flag=0;	//when from and to place are same then flag would become zero
								break;
								case 2:
								toY = "Bangalore";
								break;
								case 3:
								toY = "Delhi";
								break;
								case 4:
								toY = "Ahmedabad";
								break;
								case 5:
								toY = "Mumbai";
								break;
							}	
							if(flag == 1)	//when flag is 1 then information entered is correct so infinite for loop terminates
							{
								break;
							}
						}
						break;
						case 2:	//case 1 for flights departing form Bangalore
						fromY = "Bangalore";
						for(; ; )
						{	
							flag=1;		
							System.out.print("\n______________________________________________________\n");
							System.out.println("\tTo: ");
							displayList();
							System.out.print("______________________________________________________\n");
							System.out.print("\n->\tEnter your choice: ");
							choiceY = input.nextByte();
							System.out.print("______________________________________________________\n");
							switch(choiceY)
							{
								//various cases for arrival of flights at different cities
								case 1:
								toY = "Chennai";
								break;
								case 2:
								System.out.println("\n\t\tSOURCE AND DESTINATION PLACE CANNOT BE SAME.");
								flag = 0;
								break;
								case 3:
								toY = "Delhi";
								break;
								case 4:
								toY = "Ahmedabad";
								break;
								case 5:
								toY = "Mumbai";
								break;
							}
							if(flag == 1)
							{
								break;
							}
						}
						break;
						case 3:	//case 1 for flights departing form Delhi
						fromY = "Delhi";
						for(; ; )
						{	
							flag=1;
							System.out.print("\n______________________________________________________\n");
							System.out.println("\tTo: ");
							displayList();
							System.out.print("______________________________________________________\n");
							System.out.print("\n->\tEnter your choice: ");
							choiceY = input.nextByte();
							System.out.print("______________________________________________________\n");
							switch(choiceY)
							{
								//various cases for arrival of flights at different cities
								case 1:
								toY = "Chennai";
								break;
								case 2:
								toY = "Bangalore";
								break;
								case 3:
								System.out.println("\n\t\tSOURCE AND DESTINATION PLACE CANNOT BE SAME.");
								flag = 0;
								break;
								case 4:
								toY = "Ahmedabad";
								break;
								case 5:
								toY = "Mumbai";
								break;
							}
							if(flag == 1)
							{
								break;
							}
						}
						break;
						case 4:	//case 1 for flights departing form Ahmedabad
						fromY = "Ahmedabad";
						for(; ; )
						{
							flag=1;
							System.out.print("\n______________________________________________________\n");
							System.out.println("\tTo: ");
							displayList();
							System.out.print("______________________________________________________\n");
							System.out.print("\n->\tEnter your choice: ");
							choiceY = input.nextByte();
							System.out.print("______________________________________________________\n");
							switch(choiceY)
							{
								//various cases for arrival of flights at different cities
								case 1:
								toY = "Chennai";
								break;
								case 2:
								toY = "Bangalore";
								break;
								case 3:
								toY = "Delhi";
								break;
								case 4:
								System.out.println("\n\t\tSOURCE AND DESTINATION PLACE CANNOT BE SAME.");
								flag = 0;
								break;
								case 5:
								toY = "Mumbai";
								break;
							}
							if(flag == 1)
							{
								break;
							}
						}
						break;
						case 5:	//case 1 for flights departing form Mumbai
						fromY = "Mumbai";
						for(; ; )
						{
							flag=1;
							System.out.print("\n______________________________________________________\n");
							System.out.println("\tTo: ");
							displayList();
							System.out.print("______________________________________________________\n");
							System.out.print("\n->\tEnter your choice: ");
							choiceY = input.nextByte();
							System.out.print("______________________________________________________\n");
							switch(choiceY)
							{
								//various cases for arrival of flights at different cities
								case 1:
								toY = "Chennai";
								break;
								case 2:
								toY = "Bangalore";
								break;
								case 3:
								toY = "Delhi";
								break;
								case 4:
								toY = "Ahmedabad";
								break;
								case 5:
								System.out.println("\n\t\tSOURCE AND DESTINATION PLACE CANNOT BE SAME.");
								flag = 0;
								break;
							}
							if(flag == 1)
							{
								break;
							}
						}
						break;
					}
					System.out.print("\n\tCurrent date and time: ");
					Date date = new Date();	//to display current date and time
					System.out.println(date);
					System.out.println("\n\tEnter your journey month from the selected months: ");
					displayMonth();
					System.out.print("______________________________________________________\n");
					System.out.print("\n->\tEnter your choice: ");
					choiceMonth = input.nextByte();
					System.out.print("______________________________________________________\n");
					switch(choiceMonth)	//selecting from various months
					{
						case 1:
						startMonthY = "Jan";
						break;
						case 2:
						startMonthY = "Feb";
						break;
						case 3:
						startMonthY = "Mar";
						break;
						case 4:
						startMonthY = "Apr";
						break;
						case 5:
						startMonthY = "May";
						break;
					}
					System.out.println("\tEnter your journey time from the selected times: ");
					displayTime();
					System.out.print("______________________________________________________\n");
					System.out.print("\n->\tEnter your choice: ");
					choiceTime = input.nextByte();
					System.out.print("______________________________________________________\n");
					
					switch(choiceTime)	//selecting favourable time 
					{
						case 1:
						startTimeY = "00:00";
						break;
						case 2:
						startTimeY = "04:00";
						break;
						case 3:
						startTimeY = "08:00";
						break;
						case 4:
						startTimeY = "12:00";
						break;
						case 5:
						startTimeY = "16:00";
						break;
						case 6:
						startTimeY = "20:00";
						break;
					}
					if(fromY.equals("Chennai") && toY.equals("Bangalore") && (startTimeY.equals("00:00") || startTimeY.equals("04:00")))	//case for Chennai to Bangalore at either  12 o'clock AM or 4:00  o'clock
					{					
						System.out.println(" \n\t No of flights available: 2");
						System.out.println("\n\t1. Name: Chennai Airways  Price: 3500  Journey hours: 1:00");
						System.out.println("\t2. Name: Java Airways  Price: 3000  Journey hours: 1:15");
						System.out.print("\n\tWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)	//case for selecting airways
						{
							case 1:	
							if(choice == 2)
							{
								System.out.println("\n**********************  TICKET BOOKING  ********************\n");
								System.out.print("Do you want to book ticket(y/n)? ");	//choice for booking ticket
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								System.out.println("\n\tFlight stored in history!!");
								bookTicketBoolean = false;
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "1:00", "Chennai Airways", 3500);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "1:00", "Chennai Airways", 3500);	///type casting of f(i.e. parent class of DirectUserSearch) to DirectUserSearch
							}
							break;
							case 2:
							if(choice == 2)
							{
								System.out.println("\n**********************  TICKET BOOKING  ********************\n");
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
								{
									System.out.println("\n\tFlight stored in history!!");
									bookTicketBoolean = false;
								}
							}
							else
								bookTicketBoolean = false;
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "1:15", "Java Airways", 3000);	//type casting of f(i.e. parent class of BookTicketSearch) to BookTicketSearch
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "1:15", "Java Airways", 3000);
							}
							break;
						}
					}
					else if(fromY.equals("Chennai") && toY.equals("Delhi") && (startTimeY.equals("00:00") || startTimeY.equals("04:00") || startTimeY.equals("08:00") || startTimeY.equals("12:00") || startTimeY.equals("16:00") || startTimeY.equals("20:00"))) //case for Chennai to Delhi for all times as no flight is available
				
					{
						System.out.println("\n\t Sorry! No Flights Available due to bad weather!");
						if(choice == 2)
							((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, null, null, -999);
						else
							((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, null, null, -999);
					}
					else if(fromY.equals("Chennai") && toY.equals("Ahmedabad") && startTimeY.equals("08:00"))//case for flights from Chennai to Ahmedabad at 8 o'clock
				
					{
						System.out.println("\n\t No of flights available: 1");
						System.out.print("______________________________________________________\n");
						System.out.println("\n\t1. Name: Chennai Airways  Price: 11125  Journey hours: 2:00");
						System.out.print("\n\tWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.println("\n**********************  TICKET BOOKING  ********************\n");							
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history!!");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "2:00", "Chennai Airways", 11125);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "2:00", "Chennai Airways", 11125);
							}
							break;
						}
					}
					else if(fromY.equals("Chennai") && toY.equals("Mumbai") && startTimeY.equals("16:00"))//case for Chennai to Bangalore at 4:00 o'clock PM
				
					{
						System.out.println("\n\t No of flights available: 1");
						System.out.print("______________________________________________________\n");
						System.out.println("\n\t\t1. Name: Skyways  Price: 6500  Journey hours: 1:45");
						System.out.print("\nWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "1:45", "Skyways", 6500);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "1:45", "Skyways", 6500);
							}
							break;
						}
					}
					else if(fromY.equals("Bangalore") && toY.equals("Ahmedabad") && (startTimeY.equals("16:00")))//case for  Flights from Bangalore to Ahmedabad at 4:00 o'clock PM
					{
						System.out.println("\n\tNo of flights available: 2");
						System.out.println("\n1. Name: Skyways  Price: 11500  Journey hours: 2:30");
						System.out.println("2. Name: Ola Airways  Price: 12000  Journey hours: 2:15");
						System.out.print("\nWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								System.out.println("\n\tFlight stored in history.");
								bookTicketBoolean = false;
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "2:30", "Skyways", 11500);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "2:30", "Skyways", 11500);
							}
							break;
							case 2:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
								bookTicketBoolean = false;
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "2:15", "Ola Airways", 12000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "2:15", "Ola Airways", 12000);
							}
							break;
						}
					}
					else if(fromY.equals("Bangalore") && toY.equals("Chennai") && (startTimeY.equals("00:00") || startTimeY.equals("04:00") || startTimeY.equals("08:00") || startTimeY.equals("12:00") || startTimeY.equals("16:00") || startTimeY.equals("20:00")))//case for Bangalore to Chennai at all times as no flights are available
					{
						System.out.println("\n\tNo Flights Available due to bad weather!");
						if(choice == 2)
							((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, null, null, -999);
						else
							((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, null, null, -999);
					}
					else if(fromY.equals("Bangalore") && toY.equals("Delhi") && startTimeY.equals("04:00"))//case for Flights from Bangalore to Delhi at 4:00  o'clock AM
					{
						System.out.println("\n\tNo of flights available: 1");
						System.out.println("\n\t1. Name: Java Airways  Price: 12000  Journey hours: 2:45");
						System.out.print("\n\tWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "2:45", "Java Airways", 12000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "2:45", "Java Airways", 12000);
							}
							break;
						}
					}
					else if(fromY.equals("Bangalore") && toY.equals("Mumbai") && startTimeY.equals("20:00"))	//case for Flights from Bangalore to Mumbai at 8:00 o'clock PM
					{
						System.out.println("\n\tNo of flights available: 3");
						System.out.print("\n1. Name: Delhi Airways  Price: 6500  Journey hours: 1:30");
						System.out.print("\n2. Name: Skyways  Price: 7000  Journey hours: 1:15");
						System.out.print("\n3. Name: Java Airways  Price: 8000  Journey hours: 1:05");
						System.out.print("\nWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "1:30", "Delhi Airways", 6500);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "1:30", "Delhi Airways", 6500);
							}
							break;
							case 2:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "1:15", "Skyways", 7000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "1:15", "Skyways", 7000);
							}
							break;
							case 3:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "1:05", "Java Airways", 8000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "1:05", "Java Airways", 8000);
							}
							break;
						}
					}
					else if(fromY.equals("Delhi") && toY.equals("Chennai") && startTimeY.equals("04:00"))//case for Delhi to  Chennai at 4:00 o'clock PM
					{
						System.out.println("\n\tNo of flights available: 1");
						System.out.println("\n1. Name: Delhi Airways  Price: 11500  Journey hours: 2:45");
						System.out.print("\nWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "2:45", "Delhi Airways", 11500);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "2:45", "Delhi Airways", 11500);
							}
							break;
						}
					}
					else if(fromY.equals("Delhi") && toY.equals("Bangalore") && (startTimeY.equals("08:00")))//case for Flights from Delhi to Bangalore at 8:00 o'clock AM
					{
						System.out.println("\n\tNo of flights available: 2");
						System.out.println("\n1. Name: Skyways  Price: 10000  Journey hours: 2:45");
						System.out.println("2. Name: Java Airways  Price: 9500  Journey hours: 2:55");
						System.out.print("\nWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								System.out.println("\n\tFlight stored in history.");
								bookTicketBoolean = false;
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "2:45", "Skyways", 10000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "2:45", "Skyways", 10000);
							}
							break;
							case 2:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
								bookTicketBoolean = false;
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "2:55", "Java Airways", 9000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "2:55", "Java Airways", 9000);
							}
							break;
						}
					}
					else if(fromY.equals("Delhi") && toY.equals("Ahmedabad") && (startTimeY.equals("12:00")))//case for Delhi to Ahmedabad at 12:00 o'clock PM
					{
						System.out.println("\n\tNo of flights available: 2");
						System.out.println("\n1. Name: Delhi Airways  Price: 6500  Journey hours: 1:30");
						System.out.println("2. Name: Skyways  Price: 5000  Journey hours: 2:55");
						System.out.print("\nWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								System.out.println("\n\tFlight stored in history.");
								bookTicketBoolean = false;
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "1:30", "Delhi Airways", 6500);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "1:30", "Delhi Airways", 6500);
							}
							break;
							case 2:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
								bookTicketBoolean = false;
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "2:55", "Skyways", 5000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "2:55", "Skyways", 5000);
							}
							break;
						}
					}
					else if(fromY.equals("Delhi") && toY.equals("Mumbai") && (startTimeY.equals("00:00") || startTimeY.equals("04:00") || startTimeY.equals("08:00") || startTimeY.equals("12:00") || startTimeY.equals("16:00") || startTimeY.equals("20:00")))//case for Delhi to Mumbai at all times as no flights are available  
					{
						System.out.println("\n\tNo Flights Available due to bad weather!");
						if(choice == 2)
							((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, null, null, -999);
						else
							((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, null, null, -999);
					}
					else if(fromY.equals("Ahmedabad") && toY.equals("Chennai") && startTimeY.equals("00:00"))//case for Flights from Ahmedabad to Chennai at 12:00 o'clock AM
					{
						System.out.println("\n\tNo of flights available: 3");
						System.out.println("\n1. Name: Ahmedabad Airways  Price: 10000  Journey hours: 2:30");
						System.out.println("2. Name: Skyways  Price: 7000  Journey hours: 3:15");
						System.out.println("3. Name: Ola Airways  Price: 8000  Journey hours: 3:05");
						System.out.print("\nWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "2:30", "Ahmedabad Airways", 10000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "2:30", "Ahmedabad Airways", 10000);
							}
							break;
							case 2:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "3:15", "Skyways", 7000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "3:15", "Skyways", 7000);
							}
							break;
							case 3:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "3:05", "Ola Airways", 8000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "3:05", "Ola Airways", 8000);
							}
							break;
						}
					}
					else if(fromY.equals("Ahmedabad") && toY.equals("Bangalore") && startTimeY.equals("12:00"))//case for Ahmedabad to Bangalore at 12:00 o'clock PM
					{
						System.out.println("\n\tNo of flights available: 1");
						System.out.println("\n1. Name: Ahmedabad Airways  Price: 11000  Journey hours: 3:00");
						System.out.print("\nWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "3:00", "Ahmedabad Airways", 11000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "3:00", "Ahmedabad Airways", 11000);
							}
							break;
						}
					}
					else if(fromY.equals("Ahmedabad") && toY.equals("Delhi") && (startTimeY.equals("00:00") || startTimeY.equals("04:00") || startTimeY.equals("08:00") || startTimeY.equals("12:00") || startTimeY.equals("16:00") || startTimeY.equals("20:00")))
					{
						System.out.println("\n\tNo Flights Available due to bad weather!");
						if(choice == 2)
							((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, null, null, -999);
						else
							((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, null, null, -999);
					}
					else if(fromY.equals("Ahmedabad") && toY.equals("Mumbai") && startTimeY.equals("04:00"))//case for Flights from Chennai to Bangalore at 4:00 o'clock AM
					{
						System.out.println("\n\tNo of flights available: 1");
						System.out.println("\n1. Name: Skyways  Price: 3500  Journey hours: 1:00");
						System.out.print("\nWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "1:00", "Skyways", 3500);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "1:00", "Skyways", 3500);
							}
							break;
						}
					}
					else if(fromY.equals("Mumbai") && toY.equals("Chennai") && (startTimeY.equals("00:00") || startTimeY.equals("04:00") || startTimeY.equals("08:00") || startTimeY.equals("12:00") || startTimeY.equals("16:00") || startTimeY.equals("20:00")))//case for Mumbai to Chennai at all times as no flights are available
					{
						System.out.println("\n\tNo Flights Available due to bad weather!");
						if(choice == 2)
							((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, null, null, -999);
						else
							((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, null, null, -999);
					}
					else if(fromY.equals("Mumbai") && toY.equals("Bangalore") && startTimeY.equals("12:00"))
					{
						System.out.println("\n\tNo of flights available: 1");
						System.out.println("\n1. Name: Mumbai Airways  Price: 5500  Journey hours: 1:45");
						System.out.print("\nWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "1:45", "Mumbai Airways", 5500);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "1:45", "Mumbai Airways", 5500);
							}
							break;
						}
					}
					else if(fromY.equals("Mumbai") && toY.equals("Delhi") && startTimeY.equals("04:00"))//case for Flights from Mumbai to Delhi at 4:00 o'clock AM
					{
						System.out.println("\n\tNo of flights available: 3");
						System.out.println("\n1. Name: Ola Airways  Price: 7000  Journey hours: 2:00");
						System.out.println("2. Name: Skyways  Price: 6000  Journey hours: 2:15");
						System.out.println("3. Name: Mumbai Airways  Price: 5000  Journey hours: 3:05");
						System.out.print("\nWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "2:00", "Ola Airways", 7000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "2:00", "Ola Airways", 7000);
							}
							break;
							case 2:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "2:15", "Skyways", 6000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "2:15", "Skyways", 6000);
							}
							case 3:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "3:05", "Mumbai Airways", 5000);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "3:05", "Mumbai Airways", 5000);
							}
							break;
						}
					}
					else if(fromY.equals("Mumbai") && toY.equals("Ahmedabad") && startTimeY.equals("08:00"))//case for Mumbai to Ahmedabad at 8:00 o'clock AM
					{
						System.out.println("\n\tNo of flights available: 1");
						System.out.println("\n1. Name: Java Airways  Price: 3500  Journey hours: 1:00");
						System.out.print("\nWhich flight would you prefer? ");
						choiceAirway = input.nextByte();
						switch(choiceAirway)
						{
							case 1:
							if(choice == 2)
							{
								System.out.print("Do you want to book ticket(y/n)? ");
								bookTicketChar = input.next().charAt(0);
								if(bookTicketChar == 'y')
								{
									bookTicketBoolean = true;
									System.out.println("\n\t\tTicket Successfully booked!!");
								}
								else
									bookTicketBoolean = false;
							}
							else
							{
								bookTicketBoolean = false;
								System.out.println("\n\tFlight stored in history.");
							}
							if(choice == 2)
							{
								((BookTicketSearch)f).setData(userNameY, passwordY, mobileNoY, genderY, fromY, toY, startMonthY, startTimeY, "1:00", "Java Airways", 3500);
							}
							else
							{
								((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, "1:00", "Java Airways", 3500);
							}
							break;
						}
					}
					else
					{
						System.out.println("\n\tNo Flights available!");
						if(choice == 1)
						{
							((DirectUserSearch)f).setData(fromY, toY, startMonthY, startTimeY, null, null, -999);
						}
						else if(choice == 2)
						{
							((BookTicketSearch)f).setData(null, null, null, ' ', fromY, toY, startMonthY, startTimeY, null, null, -999);
						}
					}
					if(choice==1 || choice==2)	//if choice is 1 or 2 then data would be written to the file
					{
						System.out.print("\n\tDo you want to give feedback(y/n)? ");	//to give feedback
						char feedbackChoice = input.next().charAt(0);
						if(feedbackChoice == 'y')	//feedback input
						{
							String feedbackInput;
							feedbackInput = input.nextLine();
							System.out.println("\n**************************************************");
							System.out.print("Enter your feedback: ");
							feedbackInput = input.nextLine();
							System.out.println("\n**************************************************");
							f.setFeedback(feedbackInput);	//setting feedback
						}
						else
						{	
							f.setFeedback("Feedback not given.");	//setting feedback to 'feedback not given'
						}
						try
						{
							String string = String.valueOf(refNoY) + ".dat";	//file name
							DataOutputStream dOut = new DataOutputStream(new FileOutputStream(string));	//creating that file using DataOutputStream and FileOutputStream
							if(choice == 1)	//if choice is 1 then f would refer to DirectUserSearch
							{
								try
								{
									((DirectUserSearch)f).writeData(dOut);
									dOut.close();
								}
								catch(Exception e)
								{
									System.out.println("Error writing to file. Error desc: " + e);
								}
							}
							else if(choice == 2)	//in this case it would refer to BookTicketSearch
							{
								((BookTicketSearch)f).writeData(dOut);
								dOut.close();
							}
						}
						catch(IOException e)
						{
							System.out.println("Error writing to file. Error Desc: " + e);
						}
						/*finally
						{
							oOut.close();
						}*/
					}
				}
				if(choice == 3)	//if choice is 3 then it would search from DirectUserSearch
				{
					try
					{
						String refNoInputX;
						System.out.print("Enter your ref no: ");
						refNoInputX = input.next();
						String refNoInputS = refNoInputX + ".dat";
						DataInputStream dIn = new DataInputStream(new FileInputStream(refNoInputS));
						((DirectUserSearch)f).readData(dIn);
						f.display();
						dIn.close();
					}
					catch(Exception e)
					{
						System.out.println("Error reading from file. Error desc: " + e);
					}
				}
				if(choice == 4)	//if choice is 4 then data would be written in file of BookTicketSearch
				{
					String userNameInputX;
					String passwordInputX;
					int refNoInputZ;
					System.out.println("Enter ref no: ");
					refNoInputZ = input.nextInt();
					String refNoInputString = String.valueOf(refNoInputZ) + ".dat";
					System.out.print("Enter username: ");
					userNameInputX = input.next();
					System.out.print("Enter password: ");
					passwordInputX = input.next();
					String refNoString = String.valueOf(refNoInputZ) + ".dat";
					for(int j=0; j<refNoY+10; j++)	//i have taken refNoY+10 only for safer side //this loop is used to compare username and password
					{
						if(f.compare(userNameInputX, passwordInputX, refNoInputZ))	//calls compare function to check username and password
						{
							try
							{
								DataInputStream dIn = new DataInputStream(new FileInputStream(refNoString));	//opens file if username and password entered are correct
								((BookTicketSearch)f).readData(dIn);
								f.display();	//display data
								flag = 1;
								dIn.close();	//closing the file
								break;
							}
							catch(Exception e)
							{}
						}
					}
					if(flag != 1)
					{
						System.out.println("Invalid Input. ");
					}
				}
			}
			else if(choiceInput == 2)	//case 2 for administrator side
			{
				System.out.print("\n1. Search Direct User Search Records \n2. Search Book Ticket User Search Records. \n3. View feedbacks \n\n");
				System.out.print("______________________________________________________\n");
				System.out.print("Enter your choice: ");
				choice = input.nextByte();
				System.out.print("______________________________________________________\n");
				if(choice == 1)	//case 1 for Direct User search
				{
					System.out.print("Enter your reference no: ");
					String srefNo = input.next();
					String string = srefNo + ".dat";
					try
					{
						DataInputStream dIn = new DataInputStream(new FileInputStream(string));
						((DirectUserSearch)f).readData(dIn);
						dIn.close();
						f.display();
					}
					catch(Exception e)
					{
						System.out.println("Error reading from file. Error Desc: " + e);
					}
				}
				if(choice == 2)	//case 2 for BookTicketSearch
				{
					System.out.print("Enter your reference no: ");
					String srefNo = input.next();
					String string = srefNo + ".dat";
					try
					{
						DataInputStream dIn = new DataInputStream(new FileInputStream(string));
						((BookTicketSearch)f).readData(dIn);
						f.display();
						dIn.close();
					}
					catch(Exception e)
					{
						System.out.println("Error reading from file. Error Desc: " + e);
					}
				}
				if(choice == 3)	//case 3 for viewing feedbacks
				{
					try
					{
						System.out.print("Enter your ref no: ");
						String refNoStringX = input.next();
						String refNoStringZ = refNoStringX + ".dat";
						DataInputStream dIn = new DataInputStream(new FileInputStream(refNoStringZ));
						f.readData(dIn);
						f.displayFeedback();
						dIn.close();
					}
					catch(Exception e)
					{
						System.out.println("Error viewing feedback. Error desc: " + e);
					}
				}
			}
			else if(choiceInput == 3)
			{
				try
				{
					FileInputStream fIn = new FileInputStream("Custom.txt");
					f.viewCustoms(fIn);
				}
				catch(Exception e)
				{
					System.out.println("Error Occured. Error Desc: " + e);
				}
			}
		}while(choiceInput!=0);
		
	}
}
