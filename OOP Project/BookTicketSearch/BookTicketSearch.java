package BookTicketSearch;	//package

import FlightSearch.FlightSearch;	//importing class of FlightSearch
import java.io.*;	

public class BookTicketSearch extends FlightSearch implements Serializable	//class kept public because it is being  implemented in other class
{
	private String userName;
	private String password;
	private String mobileNo;
	private boolean bookTicketChoice;
	private char gender;
	public BookTicketSearch(int refNoX)	//parametrised Constructor
	{
		super(refNoX);	//super keyword used to call constructor of parent class and pass the value of reference no
		userName = null;
		password = null;
		mobileNo = null;
		bookTicketChoice = false;
		gender = 'm';
	}
	public void setData(String userNameX, String passwordX, String mobileNoX, char genderX, String fromX, String toX, String startMonthX, String startTimeX, String journeyTimeX, String companyNameX, double priceX)	//setting data
	{
		userName = userNameX;
		password = passwordX;
		mobileNo = mobileNoX;
		gender = genderX;
		super.setData(fromX, toX, startMonthX, startTimeX, journeyTimeX, companyNameX, priceX);
	}
	public void display()	//displaying data in the console
	{
		System.out.println("\n\tRef no		: " + refNo);
		System.out.println("\tUsername	: " + userName);
		System.out.println("\tMobile No	: " + mobileNo);
		System.out.println("\tGender		: " + gender);
		super.display();
	}
	public void writeData(DataOutputStream d)	//for writing data into the file
	{
		try
		{
			d.writeInt(refNo);
			d.writeUTF(userName);
			d.writeUTF(password);
			d.writeUTF(mobileNo);
			d.writeChar(gender);
			super.writeData(d);
		}
		catch(Exception e)
		{
			System.out.println("Error occured. Error desc: " + e);
		}
	}
	public void readData(DataInputStream d)	//to fetch data from the file
	{
		try
		{
			refNo = d.readInt();
			userName = d.readUTF();
			password = d.readUTF();
			mobileNo = d.readUTF();
			gender = d.readChar();
			super.readData(d);
		}
		catch(Exception e)
		{
			System.out.println("Error occured. Error desc: " + e);
		}
	}
}	