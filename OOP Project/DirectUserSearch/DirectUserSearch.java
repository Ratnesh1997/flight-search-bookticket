package DirectUserSearch;	//package DirectUserSearch

import FlightSearch.FlightSearch;	//importing class of FlightSearch
import java.io.*;

public class DirectUserSearch extends FlightSearch implements Serializable	//kept public so that it can be used in other class 
{
	public DirectUserSearch(int refNoX)	//parametrised constructor
	{
		super(refNoX);
	}
	public void setData(String fromX, String toX, String startMonthX, String startTimeX, String journeyTimeX, String companyNameX, double priceX)	//setting data
	{
		super.setData(fromX, toX, startMonthX, startTimeX, journeyTimeX, companyNameX, priceX);
	}
	public void display()	//displaying data
	{
		System.out.println("\n\tRef no		: " + refNo);
		super.display();
	}
	public void writeData(DataOutputStream d)	//for writing data into the file
	{
		try
		{
			d.writeInt(refNo);
			super.writeData(d);
		}
		catch(Exception e)
		{
			System.out.println("Error occured. Error desc: " + e);
		}
	}
	public void readData(DataInputStream d)	//for reading data from the file
	{
		try
		{
			refNo = d.readInt();
			super.readData(d);
		}
		catch(Exception e)
		{
			System.out.println("Error occured. Error desc: " + e);
		}
	}
}
