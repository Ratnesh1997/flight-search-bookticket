package FlightSearch;	//package FlightSearch

import java.io.*;

interface MyInterface
{
	abstract public void viewCustoms(FileInputStream f);
	abstract public void setFeedback(String feedback);
	abstract public void displayFeedback();
}

public abstract class FlightSearch implements Serializable, MyInterface	//kept abstract so that it's instance cannot be created and it is the base class which implements Serializable	//kept public so that it can be used in other classes of different package
{
	//data members are kept protected because they are being used by their immediate child classes
	protected int refNo;
	protected String from;
	protected String to;
	protected String startMonth;
	protected String startTime;
	protected String journeyTime;
	protected String companyName;
	protected double price;
	private String feedback;
	public FlightSearch(int refNoX)	//parametrised constructor
	{
		//initialising some of the variables and setting some to null
		refNo = refNoX;
		from = null;
		to = null;
		startMonth = null;
		startTime = null;
		journeyTime = null;
		companyName = null;
		price = 0;
		feedback = null;
	}
	public void setData(String fromX, String toX, String startMonthX, String startTimeX, String journeyTimeX, String companyNameX, double priceX)	//setting data
	{
		from = fromX;
		to = toX;
		startMonth = startMonthX;
		startTime = startTimeX;
		journeyTime = journeyTimeX;
		companyName = companyNameX;
		price = priceX;
	}
	public void setFeedback(String feedbackX)	//setting feedback
	{
		feedback = feedbackX;
	}
	public void display()	//to display data on console
	{
		System.out.println("\tFrom		: " + from);
		System.out.println("\tTo		: " + to);
		System.out.println("\tMonth		: " + startMonth);
		System.out.println("\tTime		: " + startTime);
		System.out.println("\tJourney Time	: " + journeyTime);
		System.out.println("\tCompany Name	: " + companyName);
		System.out.println("\tPrice		: " + price);	
		System.out.println("\tFeedback	: " + feedback);
	}
	public void displayFeedback()	//to display feedback
	{
		System.out.println("\tFeedback	: " + feedback);
	}
	public void viewCustoms(FileInputStream fIn)	//to view custom information
	{
		int f;
		try
		{
			while((f=fIn.read())!=-1)	//reads data till the end of the file
			{
				System.out.print((char)f);	//typecasting byte to char datatype
			}
		}
		catch(Exception e)
		{
			System.out.println("Error in reading from file. Error desc: " + e);
		}
	}
	public void writeData(DataOutputStream d)	//writing data into the file
	{
		try
		{
			//writeUTF to write string into the file
			d.writeUTF(from);
			d.writeUTF(to);
			d.writeUTF(startMonth);
			d.writeUTF(startTime);
			d.writeUTF(journeyTime);
			d.writeUTF(companyName);
			d.writeDouble(price);
			d.writeUTF(feedback);
		}
		catch(Exception e)
		{
			System.out.println("Error writing to file. Error desc: " + e);
		}
	}
	public void readData(DataInputStream d)	//for reading data from the file
	{
		try
		{
			//readUTF to fetch data from the file
			from = d.readUTF();
			to = d.readUTF();
			startMonth = d.readUTF();
			startTime = d.readUTF();
			journeyTime = d.readUTF();
			companyName = d.readUTF();
			price = d.readDouble();
			feedback = d.readUTF();
		}
		catch(Exception e)
		{
			System.out.println("Error reading from file. Error desc: " + e);
		}
	}
	public boolean compare(String userNameInput, String passwordInputX, int refNoInput)	//for comparing username and password that are being inputed by user and it's return type is boolean
	{
		String refNoString = String.valueOf(refNoInput) + ".dat";	//file name
		try
		{
			DataInputStream d = new DataInputStream(new FileInputStream(refNoString));	//opening that file so that username and password can be checked
			refNo = d.readInt();
			String userName = d.readUTF();
			String password = d.readUTF();
			if(userName.equals(userNameInput) && password.equals(passwordInputX))	//returns true if they are equal
				return true;
			else	//else return false
				return false;
		}
		catch(Exception e)
		{
			return false;
		}
	}
}